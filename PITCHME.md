### Extreme Capraccio

---

### What this?

##### Extreme Carpaccio is a coding game designed to encourage and favour incremental development and Continuous Delivery best practices.

---

### Preparation

* Git
* Language of choice
* Team

---

### Make teams

##### 3/4 people
##### At least one laptop per team

---

### Links

| Item                   | Reason               | Link                                 |
|---                     |---                   |---                                   |
| This presentation      | For easy link access | http://bit.ly/carpaccio-presentation |
| Slack Invite Link      | Communication        | http://bit.ly/opendays-slack         |
| Extreme Capraccio Repo | The Game             | http://bit.ly/extreme-carpaccio      |

---

### Let's go!
